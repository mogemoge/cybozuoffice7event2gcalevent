Bookmarklet: Cybozu Office 7 Event to Google Calendar
==================================================

Repository
--------------------
https://bitbucket.org/mogemoge/cybozuoffice7event2gcalevent

Home Page
--------------------
http://goo.gl/3LMEZ

License
--------------------
Apache License, Version 2.0 
http://www.apache.org/licenses/LICENSE-2.0.html

Support Browser
--------------------
Google Chrome 21 later

Contact
--------------------
mogemoge@gmail.com

