/*
  Copyright 2012 mogemoge@gmail.com

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/
(function() {
	var id = function (s) {
		return 'cybozuoffice7event2gcalevent'+'_'+s
	};
	var query = function (paramsObj) {
			var params = [];
			for (var i in paramsObj) {
				params.push(i + '=' + encodeURIComponent(paramsObj[i]));
			}
			return params.join('&');
	};
	var format_time = function(hs, ms) {
		var ampm;
		var hi = parseInt(hs);
		if (1 <= hi && hi <= 11) {
			ampm = 'am';
		} else {
			hi = (hi == 0 || hi == 12 ? 12 : hi - 12);
			ampm = 'pm';
		}
		return hi + ':' + ms + ampm;
	}
	var trim = function(s){
		return ('' + s).replace(/(^\s+)|(\s+$)/g, "");
	};
	
	var datarows = document.querySelectorAll("a[name='ScheduleData'] + table.dataView > tbody > tr > td");
	var eventData = {};
	if (datarows[0]) {
		var strdate = trim(datarows[0].textContent);
		var datedata = strdate.match(/([0-9]+) . ([0-9]+) . .* ([0-9]+) . ([0-9]+) . .* ([0-9]+) . ([0-9]+) . .* ([0-9]+) . ([0-9]+) ./);
		console.log(strdate, datedata);

		eventData["date"] = datedata[1] + "/" + datedata[2];
		eventData["from"] = format_time(datedata[3], datedata[4]);
		eventData["to"]   = format_time(datedata[7], datedata[8]);
		
	}
	if (datarows[1]) {
		eventData["title"] = trim(datarows[1].textContent);
	}
	if (datarows[2]) {
		eventData["memo"] = trim(datarows[2].textContent);
	}
	if (datarows[4]) {
		eventData["members"] = trim(datarows[4].textContent);
	}
	if (datarows[5]) {
		eventData["place"] = trim(datarows[5].textContent);
	}
	
	var url = "http://www.google.com/calendar/event?" + query({
		ctext: eventData.date + " " +eventData.from + "-" + eventData.to + " " + eventData.title + (eventData.place ? " at " + eventData.place : ''),
		action: "TEMPLATE",
	});
	
	window.open(url, id(new Date().getTime()), 'width=800, height=680, left=700');

})();
